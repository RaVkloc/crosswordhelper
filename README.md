# CrosswordHelper

![Application design](https://i.imgur.com/KF3O1MZ.png)


##### [EN]

This application is design to help you solving every cross word. 
Simple database (you can creating it using a script from `util_scripts/create_database_script.py`) stored on your disk enable to use it offline. You can easily add new expression or modify existing.

##### [PL]

Aplikacja została stworzona by ułatwić rozwiązanie każdej krzyżówki.
Dzięki prostej bazie danych (można ją stworzyć wykorzystując skrypt `util_scripts/create_database_script.py`) przechowywanej na dysku twardym komputera do używania aplikacji nie jest wymagany dostep do Internetu.
Program umożliwia wyszukiwanie haseł, dodawnie nowych oraz aktualizację już istniejących. 