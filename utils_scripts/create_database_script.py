import sqlite3

PATH = r"../back/crossword_helper.sqlite3"

conn = sqlite3.connect(PATH)

cursor = conn.cursor()

cursor.execute("""CREATE TABLE CrosswordItems ( 
    Id integer PRIMARY KEY AUTOINCREMENT,
    Answer TEXT NOT NULL UNIQUE, 
    Question TEXT NOT NULL
);""")
conn.commit()

conn.close()
