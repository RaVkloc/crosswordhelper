class CrosswordItem:
    invalid_id = -1

    def __init__(self, ident: int = invalid_id, answer: str = "", question: str = ""):
        self.id = ident
        self.answer = answer
        self.question = question
