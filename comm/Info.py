from enum import Enum


class Info:
    class InfoType(Enum):
        WARNING = 1
        ERROR = 2

    def __init__(self, text: str = "", info_type: InfoType = InfoType.ERROR):
        self.info_text = text
        self.info_type = info_type

    def set_info_text(self, text: str):
        self.info_text = text

    def set_info_type(self, info_type: InfoType):
        self.info_type = info_type
