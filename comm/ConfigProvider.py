from PyQt6.QtCore import QDir


class ConfigProvider:
    __config = dict()

    def __read_config(self):
        directory = QDir()
        # directory.cdUp()
        config_path = directory.absolutePath() + QDir.separator() + "config.cfg"

        with open(config_path, 'r') as file:
            for line in file:
                key_value_pair = line.split('=')
                self.__config[key_value_pair[0].strip()] = key_value_pair[1].strip()

    def get_param_value(self, config_param: str, default_value: str = ""):
        if len(self.__config.keys()) == 0:
            self.__read_config()

        return self.__config.get(config_param, default_value)
