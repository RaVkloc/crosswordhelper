from collections import deque

from comm.Info import Info


class InfoHolder:

    def __init__(self):
        self.info_list = deque()

    def get_info_presented(self) -> str:
        new_info_dot = "• "

        tabulation = 0
        complete_message = ""
        for info in self.info_list:
            next_info = ' ' * 4 * tabulation
            next_info += (new_info_dot + info.info_text + '\n')
            complete_message += next_info
            tabulation += 1

        return complete_message

    def prepend_info(self, info: Info):
        self.info_list.appendleft(info)

    def append_info(self, info: Info):
        self.info_list.append(info)

    def get_first_info(self):
        return self.info_list[0]

    def get_last_info(self):
        return self.info_list[-1]

    def has_info(self):
        return len(self.info_list) != 0
