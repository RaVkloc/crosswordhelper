import os

from PyQt6.QtCore import QObject
from PyQt6.QtSql import QSqlDatabase, QSqlQuery, QSqlError

from comm.ConfigProvider import ConfigProvider
from comm.CrosswordItem import CrosswordItem
from comm.Info import Info
from comm.InfoHolder import InfoHolder


class DatabaseHelper(QObject):
    def __init__(self):
        super().__init__()
        db_path = ConfigProvider().get_param_value("db_path")
        assert os.path.exists(db_path)

        self.connection = QSqlDatabase.addDatabase("QSQLITE")
        self.connection.setDatabaseName(db_path)

        assert self.connection.open()

    def search(self, answer_mask: str, question: str, result: list, info_holder: InfoHolder = None) -> bool:
        if len(answer_mask) == 0:
            answer_mask = '%'

        args = (answer_mask.lower().replace('?', '_').strip(), f"%{question.lower().strip()}%")
        query = QSqlQuery("""SELECT Id, Answer, Question
                             FROM CrosswordItems
                             WHERE Answer LIKE ? AND lower(Question) LIKE ?
                             ORDER BY Answer;""")
        for arg in args:
            query.addBindValue(arg)

        if not query.exec():
            self.__report_database_error(info_holder, query.lastError())
            info_holder.prepend_info(Info(self.tr("Unable to read required items from database.")))
            return False

        while query.next():
            result.append([query.value(index) for index in range(3)])

        return True

    def insert(self, answer: str, question: str, info_holder: InfoHolder = None) -> bool:
        if len(answer) == 0 or len(question) == 0:
            if info_holder is not None:
                info_holder.prepend_info(Info(self.tr("Empty answer or question."), Info.InfoType.ERROR))
            return False

        args = (answer.lower().strip(), question.strip())
        query = QSqlQuery("INSERT INTO CrosswordItems (Answer, Question) VALUES (?, ?);")

        for arg in args:
            query.addBindValue(arg)

        if not query.exec():
            self.__report_database_error(info_holder, query.lastError())
            if self.__check_if_error_unique(query.lastError()):
                info_holder.prepend_info(Info(self.tr("Inserting new data to database FAILED. "
                                                      "Such answer already exists."),
                                              Info.InfoType.ERROR))
            return False

        return True

    def update(self, ident: int, answer: str, question: str, info_holder: InfoHolder = None) -> bool:
        if len(answer) == 0 or len(question) == 0:
            if info_holder is not None:
                info_holder.prepend_info(Info(self.tr("Empty answer or question"), Info.InfoType.ERROR))
            return False

        args = (answer.lower().strip(), question.strip(), ident)
        query = QSqlQuery("""UPDATE CrosswordItems
                             SET Answer = ?, Question = ?
                             WHERE Id = ?;""")

        for arg in args:
            query.addBindValue(arg)

        if not query.exec():
            self.__report_database_error(info_holder, query.lastError())
            info_holder.prepend_info(Info(f"Updating question for answer '{answer}' FAILED.", Info.InfoType.ERROR))
            return False

        return True

    def update_from_item(self, item_with_changes: CrosswordItem, info_holder: InfoHolder) -> bool:
        return self.update(item_with_changes.id, item_with_changes.answer, item_with_changes.question, info_holder)

    def __report_database_error(self, info_holder: InfoHolder, error: QSqlError):
        if info_holder is not None:
            info_holder.prepend_info(Info(error.databaseText(), Info.InfoType.ERROR))

    @staticmethod
    def __check_if_error_unique(error: QSqlError) -> bool:
        return error.databaseText().startswith("UNIQUE constraint failed")
