import sys
from PyQt6.QtCore import QTranslator, QCoreApplication, QLocale
from PyQt6.QtGui import QFontDatabase, QIcon
from PyQt6.QtWidgets import QApplication

from gui.MainWindow import MainWindow

if __name__ == "__main__":
    app = QApplication(sys.argv)

    app.setWindowIcon(QIcon("gui/resources/app_icon.png"))
    # Adding custom font
    QFontDatabase.addApplicationFont("gui/resources/JBM-ExtraBold.ttf")

    # Set style to look the same on all operating systems
    app.setStyle("Fusion")

    gui_translator = QTranslator()
    if gui_translator.load(QLocale(), "gui", "_", "gui/locale"):
        QCoreApplication.installTranslator(gui_translator)

    back_translator = QTranslator()
    if back_translator.load(QLocale(), "back", "_", "back/locale"):
        QCoreApplication.installTranslator(back_translator)

    main_window = MainWindow()
    main_window.show()

    app.exec()
