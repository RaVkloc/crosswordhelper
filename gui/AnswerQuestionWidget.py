import sys

from PyQt6.QtCore import Qt, QObject, pyqtSignal
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QLineEdit, QRadioButton, QPushButton, QWidget

from back.DatabaseHelper import DatabaseHelper
from comm.CrosswordItem import CrosswordItem
from comm.Info import Info
from comm.InfoHolder import InfoHolder
from gui.InfoDialog import InfoDialog


class AnswerQuestionWidget(QWidget, QObject):
    result_data_changed = pyqtSignal(list)

    def __init__(self):
        super().__init__()

        self.answer_edit = None
        self.question_edit = None

        self.search_radio = None
        self.insert_radio = None
        self.update_radio = None

        self.button = None

        try:
            self.db_helper = DatabaseHelper()
        except AssertionError as e:
            sys.stderr.write("Unable to connect to database! Database file from config does not exists.")
            exit(1)

        self.updating_item = None

        self.create_gui()

    def create_gui(self):
        main_layout = QVBoxLayout()
        entry_layout = QHBoxLayout()

        self.answer_edit = QLineEdit()
        self.answer_edit.setPlaceholderText(self.tr("Type an answer's mask..."))
        self.answer_edit.setToolTip(self.tr("Type '?' to replace any letter. E.g. h?t fits to: hit, hot, etc."))
        self.answer_edit.setMinimumWidth(100)
        self.answer_edit.returnPressed.connect(self.__button_clicked)
        entry_layout.addWidget(self.answer_edit, 2)

        self.question_edit = QLineEdit()
        self.question_edit.setPlaceholderText(self.tr("Type a question..."))
        self.question_edit.setMinimumWidth(300)
        self.question_edit.returnPressed.connect(self.__button_clicked)
        entry_layout.addWidget(self.question_edit, 2)

        main_layout.addLayout(entry_layout)

        self.search_radio = QRadioButton(self.tr("Search answer"))
        self.search_radio.setChecked(True)
        self.search_radio.toggled.connect(self.__radio_state_changed)
        main_layout.addWidget(self.search_radio)

        self.insert_radio = QRadioButton(self.tr("Insert new answer"))
        self.insert_radio.toggled.connect(self.__radio_state_changed)
        main_layout.addWidget(self.insert_radio)

        self.update_radio = QRadioButton(self.tr("Update answer-question pair"))
        self.update_radio.toggled.connect(self.__radio_state_changed)
        main_layout.addWidget(self.update_radio)

        button_layout = QHBoxLayout()

        self.button = QPushButton(self.tr("Search"))
        # self.button.setMaximumWidth(50)
        self.button.clicked.connect(self.__button_clicked)

        button_layout.addWidget(self.button)
        button_layout.setAlignment(self.button, Qt.AlignmentFlag.AlignRight)

        main_layout.addLayout(button_layout)

        self.setLayout(main_layout)

    def __radio_state_changed(self):
        self.answer_edit.clear()
        self.question_edit.clear()

        if self.search_radio.isChecked():
            self.answer_edit.setPlaceholderText(self.tr("Type an answer's mask..."))
            self.answer_edit.setToolTip(self.tr("Type '?' to replace any letter. E.g. h?t fits to: hit, hot, etc."))
            self.button.setText(self.tr("Search"))
            self.__disable_edit_lines(False)

        elif self.insert_radio.isChecked():
            self.answer_edit.setPlaceholderText(self.tr("Type an answer..."))
            self.answer_edit.setToolTip("")
            self.button.setText(self.tr("Insert"))
            self.__disable_edit_lines(False)

        else:
            self.__prepare_update_edits()
            self.button.setText(self.tr("Update"))

    def __button_clicked(self):
        info_holder = InfoHolder()
        if self.search_radio.isChecked():
            data = []
            if not self.db_helper.search(self.answer_edit.text(), self.question_edit.text(), data, info_holder):
                self.__show_info(info_holder)

            self.result_data_changed.emit(data)
        elif self.insert_radio.isChecked():
            if not self.db_helper.insert(self.answer_edit.text(), self.question_edit.text(), info_holder):
                self.__show_info(info_holder)
                return

            self.__clear_edits_line()
        else:
            if self.updating_item is None:
                info_holder.prepend_info(Info(self.tr("No item has been selected from table."), Info.InfoType.ERROR))
                self.__show_info(info_holder)
                return

            if not self.db_helper.update(self.updating_item.id,
                                         self.answer_edit.text(),
                                         self.question_edit.text(),
                                         info_holder):
                self.__show_info(info_holder)
                return

            self.updating_item = None
            self.__clear_edits_line()
            self.__prepare_update_edits()

    def item_to_update_selected(self, item: CrosswordItem):
        self.updating_item = item

        self.__prepare_update_edits()

    def __clear_edits_line(self):
        self.answer_edit.clear()
        self.answer_edit.setFocus()
        self.question_edit.clear()

    def __disable_edit_lines(self, disabled: bool):
        self.answer_edit.setDisabled(disabled)
        self.question_edit.setDisabled(disabled)

    def __prepare_update_edits(self):
        if self.updating_item is not None:
            if self.update_radio.isChecked():
                self.answer_edit.setText(self.updating_item.answer)
                self.question_edit.setText(self.updating_item.question)
                self.__disable_edit_lines(False)
        else:
            self.__disable_edit_lines(True)

    def __show_info(self, info_holder: InfoHolder):
        if not info_holder.has_info():
            return

        dialog = InfoDialog(info_holder, self)
        dialog.exec()
