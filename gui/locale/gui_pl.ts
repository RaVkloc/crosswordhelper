<?xml version = "1.0"
encoding = "utf-8" ?
>
<!DOCTYPE TS >
<TS version = "2.1" >
<context>
    <name>AnswerQuestionWidget < /name>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "88" / >
<location filename = "..\AnswerQuestionWidget.py"
line = "43" / >
    <source>Type
an
answer
's mask...</source>
< translation > Wpisz
maskę
hasła
...
</translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "89" / >
<location filename = "..\AnswerQuestionWidget.py"
line = "44" / >
    <source>Type
'?'
to
replace
any
letter.E.g.h ? t fits
to: hit, hot, etc. < /source>
< translation > Wpisz
'?'
aby
zastąpić
dowolną
literę, np.t ? k pasuje
do: tak, tok
itp. < /translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "50" / >
    <source>Type
a
question
...
</source>
< translation > Wpisz
pytanie
...
</translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "57" / >
    <source>Search
answer < /source>
< translation > Szukaj
hasła < /translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "62" / >
    <source>Insert
new answer < /source>
< translation > Dodaj
nowe
hasło < /translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "66" / >
    <source>Update
answer - question
pair < /source>
< translation > Zmodyfikuj
hasło
lub
pytanie < /translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "90" / >
<location filename = "..\AnswerQuestionWidget.py"
line = "72" / >
<source>Search < /source>
< translation > Szukaj < /translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "94" / >
    <source>Type
an
answer
...
</source>
< translation > Wpisz
hasło
...
</translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "96" / >
<source>Insert < /source>
< translation > Dodaj < /translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "101" / >
<source>Update < /source>
< translation > Zmodyfikuj < /translation>
< /message>
< message >
<location filename = "..\AnswerQuestionWidget.py"
line = "119" / >
    <source>No
item
has
been
selected
from
table. < /source>
< translation > Nie
wybrano
żadnego
elementu
z
tabeli < /translation>
< /message>
< /context>
< context >
<name>AnswersPresentationWidget < /name>
< message >
<location filename = "..\AnswersPresentationWidget.py"
line = "28" / >
<source>Answer < /source>
< translation > Hasło < /translation>
< /message>
< message >
<location filename = "..\AnswersPresentationWidget.py"
line = "28" / >
<source>Question < /source>
< translation > Pytanie < /translation>
< /message>
< /context>
< context >
<name>MainWindow < /name>
< message >
<location filename = "..\MainWindow.py"
line = "25" / >
<source>View < /source>
< translation > Widok < /translation>
< /message>
< message >
<location filename = "..\MainWindow.py"
line = "26" / >
<source>Mode < /source>
< translation > Tryb < /translation>
< /message>
< message >
<location filename = "..\MainWindow.py"
line = "32" / >
<source>Light < /source>
< translation > Jasny < /translation>
< /message>
< message >
<location filename = "..\MainWindow.py"
line = "37" / >
    <source>Dark < /source>
    < translation > Ciemny < /translation>
    < /message>
    < /context>
    < /TS>
