from PyQt6.QtWidgets import QWidget, QVBoxLayout

from gui.AnswerQuestionWidget import AnswerQuestionWidget
from gui.AnswersPresentationWidget import AnswersPresentationWidget


class MainWidget(QWidget):
    def __init__(self, parent: QWidget = None):
        super().__init__(parent)

        self.aq_widget = None
        self.rp_widget = None

        self.create_gui()

    def create_gui(self):
        layout = QVBoxLayout()

        self.aq_widget = AnswerQuestionWidget()
        self.rp_widget = AnswersPresentationWidget()
        self.aq_widget.result_data_changed.connect(self.rp_widget.handle_new_data)
        self.rp_widget.item_selected.connect(self.aq_widget.item_to_update_selected)

        layout.addWidget(self.aq_widget)
        layout.addWidget(self.rp_widget)

        self.setLayout(layout)
