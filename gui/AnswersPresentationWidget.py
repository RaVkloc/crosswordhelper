from PyQt6.QtCore import Qt, pyqtSignal
from PyQt6.QtGui import QFont
from PyQt6.QtWidgets import QWidget, QTableWidget, QTableWidgetItem, QHBoxLayout

from comm.CrosswordItem import CrosswordItem


class AnswersPresentationWidget(QWidget):
    item_selected = pyqtSignal(CrosswordItem)

    def __init__(self):
        super().__init__()

        self.updating_gui = False
        self.table = None
        self.answer_font = None
        self.currently_presented_items: dict = {}

        self.create_gui()

    def create_gui(self):
        layout = QHBoxLayout()

        self.table = QTableWidget()
        self.table.setMaximumHeight(400)
        self.table.setColumnCount(2)
        self.table.setSortingEnabled(True)
        self.table.setHorizontalHeaderLabels([self.tr("Answer"), self.tr("Question")])
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.verticalHeader().setDefaultAlignment(Qt.AlignmentFlag.AlignCenter)
        self.table.setColumnWidth(0, 125)
        layout.addWidget(self.table)

        self.table.itemSelectionChanged.connect(self.__selected_item)

        self.setLayout(layout)

        self.answer_font = QFont("JetBrains Mono Extra Bold", 13)

    def __selected_item(self):
        selected_items = self.table.selectedItems()
        if len(selected_items) == 0:
            return

        self.item_selected.emit(self.currently_presented_items[selected_items[0].type()])

    def handle_new_data(self, data: list):
        if self.updating_gui:
            return

        self.updating_gui = True
        self.currently_presented_items.clear()
        self.table.setRowCount(len(data))

        row = 0
        counter = 1001
        for item in data:
            crossword_item = CrosswordItem(item[0], item[1], item[2])
            self.currently_presented_items[counter] = crossword_item

            self.table.setItem(row, 0, self.__get_new_answer_item_template(crossword_item.answer.upper(), counter))
            self.table.setItem(row, 1, self.__get_new_question_item_template(crossword_item.question, counter))
            row += 1
            counter += 1

        self.table.resizeRowsToContents()
        self.table.sortItems(0, Qt.SortOrder.AscendingOrder)

        self.updating_gui = False

    def __get_new_answer_item_template(self, text: str, counter: int) -> QTableWidgetItem:
        table_item = QTableWidgetItem(text, counter)
        table_item.setFont(self.answer_font)
        table_item.setTextAlignment(Qt.AlignmentFlag.AlignCenter)

        return table_item

    def __get_new_question_item_template(self, text: str, counter: int) -> QTableWidgetItem:
        question = "• " + text.replace('; ', '\n• ')

        return QTableWidgetItem(question, counter)
