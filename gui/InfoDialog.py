from PyQt6.QtWidgets import QWidget, QMessageBox

from comm.Info import Info
from comm.InfoHolder import InfoHolder


class InfoDialog(QMessageBox):
    def __init__(self, info_holder: InfoHolder, parent: QWidget = None):
        super().__init__(parent)

        self.__create_gui(info_holder)

    def __create_gui(self, info_holder: InfoHolder):
        self.setText(info_holder.get_info_presented())

        error = info_holder.get_last_info().info_type == Info.InfoType.ERROR
        self.setWindowTitle("Error" if error else "Warning")
        self.setIcon(QMessageBox.Icon.Critical if error else QMessageBox.Icon.Warning)
