from PyQt6.QtCore import Qt
from PyQt6.QtGui import QPalette, QColor, QActionGroup
from PyQt6.QtWidgets import QMainWindow, QMenuBar, QApplication

from gui.MainWidget import MainWidget


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.create_gui()

        self.__app = QApplication.instance()
        self.__default_palette = self.__app.palette()
        self.__dark_palette = self.__create_dark_palette()

    def create_gui(self):
        self.setWindowTitle("Crossword Helper")
        self.setCentralWidget(MainWidget(self))

        self.setMenuBar(self.__create_menu_bar())

    def __create_menu_bar(self) -> QMenuBar:
        menu_bar = QMenuBar()
        view_menu = menu_bar.addMenu(self.tr("View"))
        theme_menu = view_menu.addMenu(self.tr("Mode"))

        theme_group = QActionGroup(self)
        theme_group.setExclusive(True)

        light_action = theme_menu.addAction(self.tr("Light"))
        light_action.setCheckable(True)
        light_action.setChecked(True)
        light_action.triggered.connect(self.__light_mode)

        dark_action = theme_menu.addAction(self.tr("Dark"))
        dark_action.setCheckable(True)
        dark_action.triggered.connect(self.__dark_mode)

        theme_group.addAction(light_action)
        theme_group.addAction(dark_action)

        return menu_bar

    def __light_mode(self):
        self.__app.setPalette(self.__default_palette)

    def __dark_mode(self):
        self.__app.setPalette(self.__dark_palette)

    def __create_dark_palette(self) -> QPalette:
        dark_palette = QPalette()
        dark_palette.setColor(QPalette.ColorRole.Window, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.ColorRole.WindowText, Qt.GlobalColor.lightGray)
        dark_palette.setColor(QPalette.ColorRole.Base, QColor(25, 25, 25))
        dark_palette.setColor(QPalette.ColorRole.AlternateBase, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.ColorRole.ToolTipBase, Qt.GlobalColor.white)
        dark_palette.setColor(QPalette.ColorRole.ToolTipText, Qt.GlobalColor.white)
        dark_palette.setColor(QPalette.ColorRole.Text, Qt.GlobalColor.white)
        dark_palette.setColor(QPalette.ColorRole.Button, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.ColorRole.ButtonText, Qt.GlobalColor.gray)
        dark_palette.setColor(QPalette.ColorRole.BrightText, Qt.GlobalColor.red)
        dark_palette.setColor(QPalette.ColorRole.Link, QColor(42, 130, 218))
        dark_palette.setColor(QPalette.ColorRole.Highlight, QColor(42, 130, 218))
        dark_palette.setColor(QPalette.ColorRole.HighlightedText, Qt.GlobalColor.black)
        dark_palette.setColor(QPalette.ColorRole.PlaceholderText, Qt.GlobalColor.gray)

        dark_palette.setColor(QPalette.ColorGroup.Disabled, QPalette.ColorRole.Base, Qt.GlobalColor.darkGray)

        return dark_palette
